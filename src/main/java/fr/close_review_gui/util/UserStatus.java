package fr.close_review_gui.util;

public enum UserStatus {
	
	CONNECTED, DISCONNECTED, IN_COMMUNICATION;
	
}
