package fr.close_review_gui.util;

public record Couple<T1, T2>(T1 o1, T2 o2) {

}
