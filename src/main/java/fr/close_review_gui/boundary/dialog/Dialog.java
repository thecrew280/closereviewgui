package fr.close_review_gui.boundary.dialog;

import fr.close_review_gui.boundary.Boundary;
import fr.close_review_gui.controller.KernelAdapter;

public class Dialog implements Boundary {
	
	private KernelAdapter kernelAdapter;
	
	public Dialog(KernelAdapter kernelAdapter) {
		this.kernelAdapter = kernelAdapter;
	}

	@Override
	public void messageRecieved(String sourceUser, String message) {
	}
	
}
