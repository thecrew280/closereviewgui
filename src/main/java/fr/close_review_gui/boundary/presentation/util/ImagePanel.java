/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package fr.close_review_gui.boundary.presentation.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author loferga
 */
public class ImagePanel extends javax.swing.JPanel {
    
    private static final ResourceBundle bundle = ResourceBundle.getBundle("image_bundle");
    
    private BufferedImage image;
    
    /**
     * Creates new form ImagePanel
     */
    public ImagePanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    
    public void setImage(String key) {
        String resourcePath = bundle.getString(key);
        URL resourceURL = ImagePanel.class.getResource(resourcePath);
        if (resourceURL == null) {
            Logger.getLogger(ImagePanel.class.getName()).log(Level.SEVERE, "resource not found: {0}", resourcePath);
            return;
        }
        try {
            BufferedImage newImage = ImageIO.read(resourceURL);
            this.image = newImage;
            if (image == null) {
                Logger.getLogger(ImagePanel.class.getName()).log(Level.SEVERE, "no ImageReader found for {0}", resourcePath);
                return;
            }
            repaint();
        } catch (IOException ex) {
            Logger.getLogger(ImagePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (image == null) return;
        Image scaledImage = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
        g.drawImage(scaledImage, 0, 0, this);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
