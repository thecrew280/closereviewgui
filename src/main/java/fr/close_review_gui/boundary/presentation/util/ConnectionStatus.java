/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package fr.close_review_gui.boundary.presentation.util;

/**
 *
 * @author loferga
 */
public enum ConnectionStatus {
    DISCONNECTED, CONNECTED, OPENED_COMMUNICATION;
}
