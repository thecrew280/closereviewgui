/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.close_review_gui.boundary.presentation.util;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTextField;

/**
 *
 * @author loferga
 */
public class HintTextField extends JTextField {
    
    private String hint = "hint";
    
    public HintTextField() {
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent evt) {
                if (getText().equals(hint)) {
                    setText("");
                }
            }
            @Override
            public void focusLost(FocusEvent evt) {
                if (getText().length() == 0) {
                    setText(hint);
                }
            }
        });
    }
    
    public void setHint(String hint) {
        this.hint = hint;
    }
    
}
