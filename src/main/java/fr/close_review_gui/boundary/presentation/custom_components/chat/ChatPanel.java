/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package fr.close_review_gui.boundary.presentation.custom_components.chat;

/**
 *
 * @author loferga
 */
public class ChatPanel extends javax.swing.JPanel {

    /**
     * Creates new form ChatPanel
     */
    public ChatPanel() {
        initComponents();
        sendInputPanel.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chatNamePanel = new javax.swing.JPanel();
        chatNameLabel = new javax.swing.JLabel();
        chatConnectionButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        chatPanel = new javax.swing.JPanel();
        sendInputPanel = new javax.swing.JPanel();
        messageField = new javax.swing.JTextField();
        fileButton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        chatNamePanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        chatNameLabel.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        chatNameLabel.setText("Nom Conversation");

        chatConnectionButton.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        chatConnectionButton.setText("Initier une connexion");
        chatConnectionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chatConnectionButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chatNamePanelLayout = new javax.swing.GroupLayout(chatNamePanel);
        chatNamePanel.setLayout(chatNamePanelLayout);
        chatNamePanelLayout.setHorizontalGroup(
            chatNamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatNamePanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(chatNameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 585, Short.MAX_VALUE)
                .addComponent(chatConnectionButton))
        );
        chatNamePanelLayout.setVerticalGroup(
            chatNamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatNamePanelLayout.createSequentialGroup()
                .addGroup(chatNamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(chatNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                    .addComponent(chatConnectionButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        add(chatNamePanel, java.awt.BorderLayout.NORTH);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        chatPanel.setLayout(new javax.swing.BoxLayout(chatPanel, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(chatPanel);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        messageField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                messageFieldActionPerformed(evt);
            }
        });

        fileButton.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        fileButton.setText("+");
        fileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout sendInputPanelLayout = new javax.swing.GroupLayout(sendInputPanel);
        sendInputPanel.setLayout(sendInputPanelLayout);
        sendInputPanelLayout.setHorizontalGroup(
            sendInputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sendInputPanelLayout.createSequentialGroup()
                .addComponent(fileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(messageField, javax.swing.GroupLayout.DEFAULT_SIZE, 948, Short.MAX_VALUE))
        );
        sendInputPanelLayout.setVerticalGroup(
            sendInputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sendInputPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(sendInputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fileButton, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(messageField, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        add(sendInputPanel, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void messageFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_messageFieldActionPerformed
        MessagePanel messagePanel = new MessagePanel();
        messagePanel.setMessage(messageField.getText());
        chatPanel.add(messagePanel);
        messageField.setText("");
        revalidate();
        repaint();
    }//GEN-LAST:event_messageFieldActionPerformed

    private void fileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fileButtonActionPerformed
    
    private boolean isConnectionActive = false;
    
    private void chatConnectionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chatConnectionButtonActionPerformed
        if (isConnectionActive) {
            chatConnectionButton.setText("Initier une connexion");
            sendInputPanel.setVisible(false);
            isConnectionActive = false;
        } else {
            chatConnectionButton.setText("Couper la connexion"); 
            sendInputPanel.setVisible(true);
            isConnectionActive = true;
        }
    }//GEN-LAST:event_chatConnectionButtonActionPerformed
    
    public void setChatName(String name) {
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chatConnectionButton;
    private javax.swing.JLabel chatNameLabel;
    private javax.swing.JPanel chatNamePanel;
    private javax.swing.JPanel chatPanel;
    private javax.swing.JButton fileButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField messageField;
    private javax.swing.JPanel sendInputPanel;
    // End of variables declaration//GEN-END:variables
}
