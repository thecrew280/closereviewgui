package fr.close_review_gui.boundary;

public interface Boundary {
	
	void messageRecieved(String sourceUser, String message);
	
}
