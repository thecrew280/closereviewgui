package fr.close_review_gui.controller;

import java.util.Iterator;

import fr.close_review_gui.util.UserStatus;

public class JNIKernelAdapter implements KernelAdapter{
	
	private native String nativeGetUserStatus(String user);
	
	static {
		System.loadLibrary("close_review_jni");
	}

	@Override
	public UserStatus getUserStatus(String user) {
		String status = nativeGetUserStatus(user);
		return UserStatus.valueOf(status);
	}

	@Override
	public boolean loginIsValid(String login) {
		// TODO implements
		return true;
	}

	@Override
	public boolean passwordIsValid(String password) {
		// TODO implements
		return true;
	}

	@Override
	public boolean login(String login, String password) {
		// TODO implements
		return false;
	}

	@Override
	public String[] getUserList() {
		// TODO implements
		return null;
	}

	@Override
	public Iterator<Message> getHistoryWith(String user) {
		// TODO implements
		return null;
	}

	@Override
	public boolean startChat(String user) {
		// TODO implements
		return true;
	}

	@Override
	public boolean sendMessage(String message) {
		// TODO implements
		return true;
	}

	@Override
	public void leaveChat(String user) {
		// TODO implements
	}

	@Override
	public void disconnect() {
		// TODO implements
	}
	
	
	
}
