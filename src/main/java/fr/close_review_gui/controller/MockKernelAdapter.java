package fr.close_review_gui.controller;

import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import fr.close_review_gui.util.UserStatus;

public class MockKernelAdapter implements KernelAdapter {

	public boolean loginIsValid(String login) {return true;}

	public boolean passwordIsValid(String password) {return true;}

	public boolean login(String login, String password) {return true;}

	public String[] getUserList() {
		return new String[] {"ugo"};
	}

	public UserStatus getUserStatus(String user) {
		return UserStatus.CONNECTED;
	}

	public Iterator<Message> getHistoryWith(String user) {
		Message onlyMessage = new Message() {
            public String getSender() {
                return "ugo";
            }
            public Date getDate() {
                return new Date();
            }
            public String getContent() {
                return "message";
            }
		};
		return Collections.singleton(onlyMessage).iterator();
	}

	public boolean startChat(String user) {return true;}

	public boolean sendMessage(String message) {return true;}

	public void leaveChat(String user) {/**/}

	public void disconnect() {/**/}

}
