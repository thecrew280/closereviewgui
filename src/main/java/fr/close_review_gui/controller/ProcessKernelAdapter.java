package fr.close_review_gui.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import fr.close_review_gui.util.UserStatus;

public class ProcessKernelAdapter implements KernelAdapter {
	
	private static final String BINARY_PATH = "./bin/close_review";
	private static final String SERVER_IP = "127.0.0.1";
	private static final String SERVER_PORT = "5556";
	
	private Process process;
	
	public ProcessKernelAdapter() {
		try {
			process = Runtime.getRuntime().exec(new String[] {
					BINARY_PATH,
					SERVER_IP,
					SERVER_PORT
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean loginIsValid(String login) {
		// TODO implements
		return true;
	}

	@Override
	public boolean passwordIsValid(String password) {
		// TODO implements
		return true;
	}

	@Override
	public boolean login(String login, String password) {
		String command = String.format("/connect %s %s", login, password);
		boolean err = true;
		try {
			process.getOutputStream().write(command.getBytes());
            process.getOutputStream().flush();
			err = process.getErrorStream().available() == 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return err;
	}

	@Override
	public String[] getUserList() {
		String command = "/list";
		List<String> outputLines = new ArrayList<>();
        try (ExecutorService executor = Executors.newSingleThreadExecutor()) {
            // Send the command to the process
            process.getOutputStream().write(command.getBytes());
            process.getOutputStream().flush();

            // Wait for the specified timeout
            Future<List<String>> future = executor.submit(() -> {
                List<String> lines = new ArrayList<>();
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
                return lines;
            });

            outputLines = future.get(1, TimeUnit.SECONDS);
            executor.shutdown();

        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return outputLines.toArray(new String[outputLines.size()]);
	}

	@Override
	public UserStatus getUserStatus(String user) {
        String command = String.format("/getstatus %s", user);
        try (ExecutorService executor = Executors.newSingleThreadExecutor()) {
            // Send the command to the process
            process.getOutputStream().write(command.getBytes());
            process.getOutputStream().flush();

            // Wait for the specified timeout
            Future<UserStatus> future = executor.submit(() -> {
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String output = reader.readLine();
                // Parse the output to get the user's status
                return UserStatus.valueOf(output);
            });

            return future.get(1, TimeUnit.SECONDS);

        } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return UserStatus.DISCONNECTED;
	}

	@Override
	public Iterator<Message> getHistoryWith(String user) {
		// TODO implements
		return null;
	}

	@Override
	public boolean startChat(String user) {
		// TODO implements
		return false;
	}

	@Override
	public boolean sendMessage(String message) {
		// TODO implements
		return false;
	}

	@Override
	public void leaveChat(String user) {
		// TODO implements
	}

	@Override
	public void disconnect() {
		// TODO implements
	}

}
