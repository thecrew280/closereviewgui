package fr.close_review_gui.controller;

import java.util.Date;
import java.util.Iterator;

import fr.close_review_gui.util.UserStatus;

public interface KernelAdapter {
	
	public static interface Message {
		String getSender();
		
		Date getDate();
		
		String getContent();
	}
	
	boolean loginIsValid(String login);
	
	boolean passwordIsValid(String password);
	
	boolean login(String login, String password);
	
	String[] getUserList();
	
	UserStatus getUserStatus(String user);
	
	Iterator<Message> getHistoryWith(String user);
	
	boolean startChat(String user);
	
	boolean sendMessage(String message);
	
	void leaveChat(String user);
	
	void disconnect();
	
}
