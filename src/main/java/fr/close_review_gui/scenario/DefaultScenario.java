/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.close_review_gui.scenario;

import com.formdev.flatlaf.FlatLightLaf;
import fr.close_review_gui.boundary.presentation.LoginDialog;
import fr.close_review_gui.boundary.presentation.MainFrame;
import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author loferga
 */
public class DefaultScenario extends Scenario {
    
    public DefaultScenario() {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(DefaultScenario.class.getName()).log(Level.SEVERE, null, ex);
        }
        MainFrame mainFrame = new MainFrame();
        LoginDialog loginDialog = new LoginDialog(mainFrame, true);
        EventQueue.invokeLater(() -> loginDialog.setVisible(true));
    }
    
    public static void main(String[] args) {
        DefaultScenario scenario = new DefaultScenario();
        scenario.start();
    }
    
}
